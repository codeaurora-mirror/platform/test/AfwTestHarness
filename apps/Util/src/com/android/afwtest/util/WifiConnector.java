/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.afwtest.util;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.SystemClock;
import android.util.Log;

import com.android.afwtest.common.NetworkUtils;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * Utility class responsible for connecting wifi.
 */
public final class WifiConnector implements NetworkMonitor.Callback {

    private static final String TAG = "afwtest.WifiConnector";

    /** Connection retry base duration. */
    private static final int RETRY_SLEEP_DURATION_BASE_MS = 500;
    /** Connection retry duration multiplier. */
    private static final int RETRY_SLEEP_MULTIPLIER = 2;
    /** Max connection attempts. */
    private static final int MAX_ATTEMPS = 6;
    /** Wifi reconnection timeout. */
    private static final int RECONNECT_TIMEOUT_MS = (int)TimeUnit.MINUTES.toMillis(1);

    private final Context mContext;
    private final String mSSID, mSecurityType, mPassword;
    private final WifiManager mWifiManager;
    private final WifiConfig mWifiConfig;

    /**
     * {@link Semaphore}, indicating result is available if it's value > 0.
     */
    private Semaphore mLock = new Semaphore(0);

    /**
     * Constructor.
     *
     * @param context {@link Context} object
     * @param ssid Wifi SSID
     * @param securityType Wifi security type
     * @param password Wifi password
     */
    public WifiConnector(Context context, String ssid, String securityType, String password) {
        mContext = context;
        mSSID = ssid;
        mSecurityType = securityType;
        mPassword = password;
        mWifiManager  = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        mWifiConfig = new WifiConfig(mWifiManager);
    }

    /**
     * Connects to WIFI.
     *
     * @return {@code true} if the given WIFI is connected; {@code false} othwerise
     */
    public boolean connect() {
        if (!NetworkUtils.enableWifi(mContext)) {
            Log.e(TAG, "Failed to enable WIFI.");
            return false;
        }

        NetworkMonitor networkMonitor = new NetworkMonitor(mContext, this);

        try {
            int netId = -1;

            int nextSleepTimeMs = RETRY_SLEEP_DURATION_BASE_MS;
            int attempts = MAX_ATTEMPS;
            while (attempts > 0) {
                if (netId == -1) {
                    netId = mWifiConfig.addNetwork(mSSID, mSecurityType, mPassword);
                }

                if (netId == -1) {
                    Log.e(TAG, "Failed to save network.");
                } else if (!mWifiManager.reconnect()) {
                    Log.e(TAG, "Unable to connect to wifi");
                } else {
                    // Connected
                    break;
                }

                --attempts;
                // Sleep before next attempt
                Log.e(TAG, String.format("Retrying in %s ms.", nextSleepTimeMs));
                SystemClock.sleep(nextSleepTimeMs);
                // Increase the next sleep time.
                nextSleepTimeMs *= RETRY_SLEEP_MULTIPLIER;
            }

            // Failed to add network or reconnect.
            if (netId == -1 || attempts <= 0) {
                return false;
            }

            // Wait for connection event
            mLock.tryAcquire(RECONNECT_TIMEOUT_MS, TimeUnit.MILLISECONDS);
            return true;
        } catch (InterruptedException e) {
            Log.e(TAG, "Failed to connect to wifi", e);
        } finally {
            networkMonitor.close();
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onNetworkConnected() {
        if (NetworkUtils.isConnectedToSpecifiedWifi(mContext, mSSID)) {
            // Let's the waiter know it's connected.
            mLock.release();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onNetworkDisconnected() {
    }
}