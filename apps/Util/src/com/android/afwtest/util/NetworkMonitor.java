/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.util.Log;

import com.android.afwtest.common.NetworkUtils;

/**
 * Monitor the state of the data network. Invoke a callback when the network is connected
 */
public class NetworkMonitor {
    private static final String TAG = "afwtest.NetworkMonitor";

    /** State notification callback. Expect some duplicate notifications. */
    public interface Callback {
        /** Notify on network connected. */
        void onNetworkConnected();
        /** Notify on network disconnected. */
        void onNetworkDisconnected();
    }

    /** Application context. */
    private final Context mContext;

    /** Registered callback that is listening to network events. */
    private final Callback mCallback;

    /** Whether receiver is registered. */
    private boolean mReceiverRegistered;

    /**
     * Start watching the network and monitoring the checkin service. Immediately invokes one of the
     * callback methods to report the current state, and then invokes callback methods over time as
     * the state changes.
     *
     * @param context to use for intent observers and such
     * @param callback to invoke when the network status changes
     */
    public NetworkMonitor(Context context, Callback callback) {
        mContext = context;
        mCallback = callback;

        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

        context.registerReceiver(mBroadcastReceiver, filter);

        mReceiverRegistered = true;
    }

    /**
     * Stop watching the network and checkin service.
     */
    public synchronized void close() {
        if (mReceiverRegistered) {
            mContext.unregisterReceiver(mBroadcastReceiver);
            mReceiverRegistered = false;
        }
    }

    /**
     * Broadcast receiver.
     */
    public final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, String.format("onReceive: %s", intent.toString()));

            if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                if (NetworkUtils.isConnectedToWifi(context)) {
                    mCallback.onNetworkConnected();
                } else {
                    mCallback.onNetworkDisconnected();
                }
            }
        }
    };
}
