/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.util;

import android.app.Activity;
import android.app.Instrumentation;
import android.os.Bundle;
import android.util.Log;

import com.android.afwtest.common.NetworkUtils;

/**
 * Adds a wifi network to system.
 */
public class Wifi extends Instrumentation {

    private static final String TAG = "afwtest.Wifi";

    private static final String ACTION_CONNECT = "connect";
    private static final String ACTION_DISCONNECT = "disconnect";

    /** Supported actions: connect, disconnect*/
    private String mAction;

    private String mSSID, mSecurityType, mPassword;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(Bundle arguments) {
        super.onCreate(arguments);
        mAction = arguments.getString("action", "");
        mSSID = arguments.getString("ssid", "");
        if (!mSSID.startsWith("\"") || !mSSID.endsWith("\"")) {
            mSSID = String.format("\"%s\"", mSSID);
        }
        mSecurityType = arguments.getString("security_type", "");
        mPassword = arguments.getString("password", "");
        start();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onStart() {
        // Handle connect action
        if (ACTION_CONNECT.equals(mAction)) {
            handleConnect();
        } else if (ACTION_DISCONNECT.equals(mAction)) {
            handleDisconnect();
        } else {
            handleError(String.format("Invalid action: %s", mAction));
        }
    }

    /**
     * Handles connecting wifi request.
     */
    private void handleConnect() {
        if (!NetworkUtils.enableWifi(getContext())) {
            handleError("Failed to enable wifi.");
        }

        // If the expected wifi is already connected, return.
        if (NetworkUtils.isConnectedToSpecifiedWifi(getContext(), mSSID)) {
            Log.i(TAG, String.format("Wifi already connected: %s (%s)", mSSID, mSecurityType));
            handleSuccess();
        } else {
            WifiConnector wifiConnector = new WifiConnector(getContext(),
                    mSSID,
                    mSecurityType,
                    mPassword);
            if (wifiConnector.connect()) {
                Log.i(TAG, String.format("Connected to wifi: %s (%s)", mSSID, mSecurityType));
                handleSuccess();
            } else {
                handleError(String.
                        format("Failed to connect to wifi: %s (%s)", mSSID, mSecurityType));
            }
        }
    }

    /**
     * Handles wifi disconnecting request.
     *
     * Handled by removing all configured networks.
     */
    private void handleDisconnect() {
        if (!NetworkUtils.disconnectFromWifi(getContext())) {
            handleError("Failed to disconnect from Wifi");
        } else {
            Log.i(TAG, "Wifi disconnected.");
            handleSuccess();
        }
    }

    /**
     * Handles error by exiting instrumentation.
     *
     * @param errorMsg error msg
     */
    private void handleError(String errorMsg) {
        Log.i(TAG, errorMsg);
        Bundle results = new Bundle();
        results.putString("error", errorMsg);
        finish(Activity.RESULT_CANCELED, results);
    }

    /**
     * Handles success execution
     */
    private void handleSuccess() {
        Bundle results = new Bundle();
        results.putString("result", "SUCCESS");
        finish(Activity.RESULT_OK, results);
    }
}