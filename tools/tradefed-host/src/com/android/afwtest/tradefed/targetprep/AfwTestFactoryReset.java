/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep;

import com.android.afwtest.tradefed.utils.TimeUtil;
import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.targetprep.ITargetPreparer;
import com.android.tradefed.targetprep.TargetSetupError;

import java.util.concurrent.TimeUnit;

/**
 * A {@link ITargetPreparer} that factory resets the device by sending an intent
 * to package com.android.afwtest.deviceadmin (AfwThDeviceAdmin.apk).
 *
 * <p>Requires a device where 'adb' is available after factory reset, typically
 * a userdebug build type.
 * </p>
 */
@OptionClass(alias = "afw-test-factory-reset")
public class AfwTestFactoryReset extends AfwTestTargetPreparer implements ITargetPreparer {

    @Option(name = "use-fastboot",
            description = "Whether to use 'fastboot format' to do factory reset")
    private boolean mUseFastboot = false;

    @Option(name = "timeout-secs",
            description = "Factory reset timeout, in seconds.")
    private Long mTimeoutSecs = TimeUnit.MINUTES.toSeconds(15);

    @Option(name = "attempts",
            description = "Number of attempts to start factory reset")
    private int mAttempts = 2;

    @Option (name = "wipe-protection-data",
            description = "If factory reset protection data should be wiped")
    private boolean mWipeProtectionData = false;

    // Command string to change system locale to en_US
    private static final String CHANGE_LOCALE_TO_EN_US_CMD =
            "am instrument -w -e locale en_US com.android.afwtest.systemutil/.ChangeLocale";

    // After sending the factory reset intent out, time allowed for
    // the device to start rebooting
    private static final long REBOOT_WAIT_TIME_MS = TimeUnit.MINUTES.toMillis(2);

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp(ITestDevice device, IBuildInfo buildInfo) throws TargetSetupError,
            DeviceNotAvailableException {
        if (getUseFastboot(device)) {
            CLog.i("Factory resetting device via fastboot");
            wipeDevice(device);
        } else {
            CLog.i("Factory resetting device via DeviceAdmin app");
            doFactoryReset(device);
        }

        // Wait for the device to be available
        long factoryResetTimeoutMs = getFactoryResetTimeout();
        CLog.i(String.format("Waiting for factory reset to finish: timeout=%d seconds",
                factoryResetTimeoutMs / 1000));
        device.waitForDeviceAvailable(factoryResetTimeoutMs);

        // re-enable adb root
        enableAdbRoot(device);

        CLog.i(String.format("Device %s is factory reset successfully", device.getSerialNumber()));

        postFactoryReset(device);
    }

    /**
     * Gets if configured to use fastboot to do factory reset.
     *
     * @return {@code true} if use fastboot, {@code false} otherwise
     */
    protected boolean getUseFastboot(ITestDevice device) throws DeviceNotAvailableException {
        return mUseFastboot;
    }

    /**
     * Executes actions after factory reset.
     */
    protected void postFactoryReset(ITestDevice device)
            throws DeviceNotAvailableException, TargetSetupError {
        // Sync time with the host
        TimeUtil.syncHostTime(device);

        CLog.i("Disabling auto-rotate");
        device.executeShellCommand("settings put system accelerometer_rotation 0");

        // Change system locale to en_US
        CLog.i("Changing system locale to en_US");
        String result = device.executeShellCommand(CHANGE_LOCALE_TO_EN_US_CMD);
        if (result == null || !result.contains("result=SUCCESS")) {
            CLog.e(result);
        } else {
            CLog.i("System locale changed to en_US");
        }
    }

    /**
     * Gets configured factory reset time out, in ms.
     *
     * @return factory reset timeout, in ms
     */
    private long getFactoryResetTimeout() {
        long result = 0;
        // Get from afw-test.props file
        if (getTestConfig() != null) {
            result = TimeUnit.MINUTES.toMillis(getTestConfig().getFactoryResetTimeoutMin(0));
        }
        // If not specified in afw-test.props, use mTimeoutSecs
        if (result == 0) {
            result = TimeUnit.SECONDS.toMillis(mTimeoutSecs);
        }

        return result;
    }

    /**
     * Does factory reset with device admin.
     *
     * @param device test device
     */
    private void doFactoryReset(ITestDevice device)
            throws DeviceNotAvailableException, TargetSetupError {
        int count = 0;
        boolean factoryResetStarted;
        do {
            count++;
            CLog.i(String.format("Factory resetting device %s, #%d",
                    device.getSerialNumber(), count));

            CLog.i(device.executeShellCommand(getFactoryResetCommand()));
            factoryResetStarted = device.waitForDeviceNotAvailable(REBOOT_WAIT_TIME_MS);
        } while (count < mAttempts && !factoryResetStarted);

        // Wait for the device to reboot
        if (!factoryResetStarted) {
            throw new TargetSetupError(
                    "Failed to start factory reset on device " + device.getSerialNumber());
        }
    }

    /**
     * Gets factory reset command string.
     */
    private String getFactoryResetCommand() {
        String result = "am start -n com.android.afwtest.deviceadmin/.FactoryResetActivity";
        if (mWipeProtectionData) {
            result += " --ez afwtest.wipe.protection.data true";
        }
        return result;
    }
}

