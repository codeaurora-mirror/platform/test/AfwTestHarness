/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep.metrics;

import static com.android.afwtest.tradefed.targetprep.metrics.nano.Metrics.InvocationMetrics;
import static com.android.afwtest.tradefed.targetprep.metrics.nano.Metrics.InvocationMetrics.InvocationMetric;
import static com.android.afwtest.tradefed.targetprep.metrics.nano.Metrics.InvocationMetrics.MetricValue;
import static com.android.internal.logging.nano.MetricsProto.MetricsEvent;

import com.android.afwtest.tradefed.utils.StreamDiscarder;
import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.device.ITestDevice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Collector of the metrics from logcat events. */
public class LogcatMetricsCollector implements MetricsCollector {

    // Logcat process from which events are read.
    private Process mLogcatProcess;

    // Thread of the event parser.
    private Thread mParserMain;

    // Thread to read and discard logcat's stderr to prevent pipe buffer overflow.
    private Thread mLogcatErrDiscarder;

    // Map of logcat events to collect to AfW TH specific metric types.
    private final Map<Integer, Integer> mEventsToCollect = new HashMap<>();

    // Initializer of set of logcat event to collect as metrics.
    {
        mEventsToCollect.put(
                MetricsEvent.PROVISIONING_TOTAL_TASK_TIME_MS,
                InvocationMetrics.TOTAL_PROVISIONING_TIME_MS);
    }

    /** {@inheritDoc} */
    @Override
    public void init(
            ITestDevice iTestDevice,
            IBuildInfo iBuildInfo,
            Consumer<InvocationMetric> metricsConsumer)
            throws MetricsCollectorException {

        try {
            startLogcat(iTestDevice.getSerialNumber());
            mParserMain =
                    new Thread(
                            new LogcatEventParserMain(
                                    mLogcatProcess.getInputStream(),
                                    (eventId, value) -> {
                                        if (mEventsToCollect.containsKey(eventId)) {
                                            final InvocationMetric metric = new InvocationMetric();
                                            final MetricValue metricValue = new MetricValue();

                                            metricValue.setStringValue(value);

                                            metric.metricType = mEventsToCollect.get(eventId);
                                            metric.setScalarValue(metricValue);
                                            metricsConsumer.accept(metric);
                                        }
                                    }));
            mParserMain.start();
            mLogcatErrDiscarder = new Thread(new StreamDiscarder(mLogcatProcess.getErrorStream()));
            mLogcatErrDiscarder.start();
        } catch (Exception e) {
            destroy();
            throw new MetricsCollectorException(
                    String.format("Failed to initialize %s.", getClass().getName()), e);
        }
    }

    /** {@inheritDoc} */
    @Override
    public void destroy() {
        if (mLogcatProcess != null) {
            mLogcatProcess.destroy();
            try {
                mLogcatProcess.waitFor();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        if (mParserMain != null) {
            try {
                mParserMain.join();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        if (mLogcatErrDiscarder != null) {
            try {
                mLogcatErrDiscarder.join();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    /**
     * Starts new logcat process to capture events from device.
     *
     * @param serial serial number of a device.
     * @throws MetricsCollectorException if could not create new logcat process.
     */
    private void startLogcat(String serial) throws MetricsCollectorException {
        ProcessBuilder pb = new ProcessBuilder("adb", "-s", serial, "logcat", "-b", "events");
        try {
            mLogcatProcess = pb.start();
        } catch (IOException e) {
            throw new MetricsCollectorException("Could not start logcat to capture events.", e);
        }
    }

    /** {@link Runnable} for a thread to parse logcat output. */
    private static final class LogcatEventParserMain implements Runnable {

        // Sample event string:
        // 03-01 14:30:12.634   788  2845 I sysui_action: [325,4706]
        private static final Pattern EVENT_PATTERN =
                Pattern.compile("sysui_action: \\[(\\d+),(.+)\\]");

        private final InputStream mStream;
        private final BiConsumer<Integer, String> mEventConsumer;

        LogcatEventParserMain(InputStream stream, BiConsumer<Integer, String> eventConsumer) {
            mStream = stream;
            mEventConsumer = eventConsumer;
        }

        @Override
        public void run() {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(mStream))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    Matcher matcher = EVENT_PATTERN.matcher(line);
                    if (!matcher.find()) {
                        continue;
                    }
                    mEventConsumer.accept(Integer.parseInt(matcher.group(1)), matcher.group(2));
                }
            } catch (IOException e) {
                throw new RuntimeException("Cannot read logcat output.", e);
            }
        }
    }
}
