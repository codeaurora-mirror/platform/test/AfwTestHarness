/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed.targetprep;

import com.android.tradefed.build.IBuildInfo;
import com.android.tradefed.config.Option;
import com.android.tradefed.config.OptionClass;
import com.android.tradefed.device.DeviceNotAvailableException;
import com.android.tradefed.device.ITestDevice;
import com.android.tradefed.log.LogUtil.CLog;
import com.android.tradefed.targetprep.ITargetCleaner;
import com.android.tradefed.targetprep.TargetSetupError;
import com.android.tradefed.util.RunUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * A {@link ITargetPreparer} that connects to wifi network with configurations read from file.
 */
@OptionClass(alias = "afw-test-wifi")
public class AfwTestWifiPreparer extends AfwTestTargetPreparer implements ITargetCleaner {

    /**
     * Package name of the system util app.
     */
    private static final String UTIL_PKG_NAME = "com.android.afwtest.util";

    /** Shell command template for connect/disconnect wifi. */
    private static final String ADB_SHELL_CMD_TEMPL =
            "am instrument -w %s com.android.afwtest.util/.Wifi";

    @Option(name = "wifi-config-file",
            description = "The file name containing wifi configuration.")
    private String mConfigFileName = null;

    @Option(name = "wifi-ssid-key",
            description = "The key of wifi ssid in the config file.")
    private String mWifiSsidKey = null;

    @Option(name = "wifi-security-type-key",
            description = "The key of wifi security type in the config file.")
    private String mWifiSecurityType = null;

    @Option(name = "wifi-password-key",
            description = "The key of wifi password in the config file.")
    private String mWifiPasswordKey = null;

    @Option(name = "wifi-attempts",
            description = "Number of attempts to connect to wifi network.")
    private int mWifiAttempts = 5;

    @Option(name = "disconnect-wifi-after-test",
            description = "Disconnect from wifi network after test completes.")
    private boolean mDisconnectWifiAfterTest = true;

    /** Whether to connect with AfwThUtil app. */
    private boolean mWifiConnectedWithUtilApp = false;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp(ITestDevice device, IBuildInfo buildInfo) throws TargetSetupError,
            DeviceNotAvailableException {

        WifiConfig wifiConfig = getWifiConfig(buildInfo);

        // tradefed doesn't support WEP wifi; connect to it by AfwThUtil
        if ("WEP".equals(wifiConfig.getSecurityType())) {
            mWifiConnectedWithUtilApp = true;
            connectWifiWithUtilApp(device,
                    wifiConfig.getSSID(),
                    wifiConfig.getSecurityType(),
                    wifiConfig.getSecurityKey());
            return;
        }

        // Implement retry.
        for (int i = 0; i < mWifiAttempts; ++i) {
            try {
                if (device.connectToWifiNetworkIfNeeded(
                        wifiConfig.getSSID(), wifiConfig.getSecurityKey())) {
                    return;
                }
            } catch (Exception e) {
                CLog.e(e);
            }
            boolean lastAttempt = (i + 1) == mWifiAttempts;
            if (!lastAttempt) {
                RunUtil.getDefault().sleep(device.getOptions().getWifiRetryWaitTime());
            }
        }

        throw new TargetSetupError(String.format("Failed to connect to wifi network %s on %s",
                wifiConfig.getSSID(), device.getSerialNumber()));
    }

    /**
     * Loads configuration of Wi-Fi to be used.
     *
     * @param buildInfo data about the build under test.
     * @return loaded Wi-Fi configuration.
     */
    protected WifiConfig getWifiConfig(IBuildInfo buildInfo) throws TargetSetupError {
        if (mConfigFileName == null) {
            throw new TargetSetupError("wifi-config-file not specified");
        }

        if (mWifiSsidKey == null) {
            throw new TargetSetupError("wifi-ssid-key not specified");
        }

        File configFile;
        try {
            configFile = new File(getCtsBuildHelper(buildInfo).getTestsDir(), mConfigFileName);
        } catch (FileNotFoundException e) {
            throw new TargetSetupError("Couldn't find tests directory");
        }
        Properties props;
        try {
            props = readProperties(configFile);
        } catch (IOException e) {
            throw new TargetSetupError(
                    String.format("Failed to read prop file: %s", configFile.getAbsolutePath()));
        }

        String wifiSsid = props.getProperty(mWifiSsidKey, "");
        String wifiSecurityType = props.getProperty(mWifiSecurityType, "");
        String wifiPassword = props.getProperty(mWifiPasswordKey, "");

        if (wifiSsid.isEmpty()) {
            throw new TargetSetupError(String.format(
                    "%s not specified in file %s", mWifiSsidKey, configFile.getAbsolutePath()));
        }

        return new WifiConfig(wifiSsid, wifiSecurityType, wifiPassword);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tearDown(ITestDevice device, IBuildInfo buildInfo, Throwable e)
            throws DeviceNotAvailableException {

        if (mDisconnectWifiAfterTest && device.isWifiEnabled()) {
            if (mWifiConnectedWithUtilApp) {
                disconnectWifiWithUtilApp(device);
            } else {
                if (device.disconnectFromWifi()) {
                    CLog.i("Successfully disconnected from wifi network on %s",
                            device.getSerialNumber());
                } else {
                    CLog.w("Failed to disconnect from wifi network on %s",
                            device.getSerialNumber());
                }
            }
        }
    }

    /**
     * Connects to wifi with AfwThUtil app.
     *
     * @param device test device
     * @param wifiSSID WIFI SSID
     * @param securityType WIFI security type
     * @param password WIFI password
     */
    private void connectWifiWithUtilApp(ITestDevice device,
            String wifiSSID,
            String securityType,
            String password)
            throws TargetSetupError, DeviceNotAvailableException {
        String args = String.format("-e action connect -e ssid %s", wifiSSID);
        if (securityType != null && !securityType.isEmpty()) {
            args = String.format("%s -e security_type %s", args, securityType);
        }
        if (password != null && !password.isEmpty()) {
            args = String.format("%s -e password %s", args, password);
        }

        String cmdStr = String.format(ADB_SHELL_CMD_TEMPL, args);
        CLog.i(String.format("Shell: %s", cmdStr));
        String result = device.executeShellCommand(cmdStr);
        if (result != null && result.contains("result=SUCCESS")) {
            CLog.i(String.format(
                    "Successfully connected to wifi network %s(security_type=%s) on %s",
                    wifiSSID,
                    securityType,
                    device.getSerialNumber()));
        } else {
            throw new TargetSetupError(String.format(
                    "Failed to connected to wifi network %s(security_type=%s) on %s: %s",
                    wifiSSID,
                    securityType,
                    device.getSerialNumber(),
                    result));
        }
    }

    /**
     * Disconnects from Wifi with AfwThUtil app.
     *
     * @param device test device
     */
    private void disconnectWifiWithUtilApp(ITestDevice device) throws DeviceNotAvailableException {
        String cmdStr = String.format(ADB_SHELL_CMD_TEMPL, "-e action disconnect");
        CLog.i(String.format("Shell: %s", cmdStr));
        String result = device.executeShellCommand(cmdStr);
        if (result != null && result.contains("result=SUCCESS")) {
            CLog.i(String.format("Successfully disconnected from wifi network on %s",
                    device.getSerialNumber()));
        } else {
            CLog.w(String.format("Failed to disconnect from wifi network on %s: %s",
                    device.getSerialNumber(),
                    result));
        }
    }

    /**
     * Help function to read {@link Properties} from a file.
     *
     * @param file {@link File} to read properties from
     * @return {@link Properties} read from given file
     *
     * @throws IOException if read failed
     */
    private static Properties readProperties(File file) throws IOException {
        InputStream input = null;

        try {
            input = new FileInputStream(file);
            Properties props = new Properties();
            props.load(input);
            return props;
        } finally {
            if (input != null) {
                input.close();
            }
        }
    }

    /**
     * Helper class to hold Wi-Fi configuration.
     */
    protected final static class WifiConfig {
        private final String mSSID;
        private final String mSecurityType;
        private final String mSecurityKey;

        /**
         * Constructs new instance of Wi-Fi configuration holder.
         *
         * @param ssid SSID of Wi-Fi access point.
         * @param securityType security type used by Wi-Fi access point.
         * @param securityKey security key used by Wi-Fi access point.
         */
        protected WifiConfig(String ssid, String securityType, String securityKey) {
            this.mSSID = ssid;
            this.mSecurityType = securityType;
            this.mSecurityKey = securityKey;
        }

        /**
         * Returns SSID of Wi-Fi access point.
         */
        protected String getSSID() {
            return mSSID;
        }

        /**
         * Returns security type used by Wi-Fi access point.
         */
        protected String getSecurityType() {
            return mSecurityType;
        }

        /**
         * Returns security key used by Wi-Fi access point.
         */
        protected String getSecurityKey() {
            return mSecurityKey;
        }
    }
}

