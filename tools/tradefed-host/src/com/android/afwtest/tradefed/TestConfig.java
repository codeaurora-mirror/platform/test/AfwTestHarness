/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.tradefed;

import com.android.tradefed.log.LogUtil.CLog;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * A singleton class that reads properties from afw-test.props and provides interfaces
 * to get test configurations.
 */
public class TestConfig {
    /**
     * Property key of factory reset timeout in minutes.
     */
    public static final String KEY_FACTORY_RESET_TIMEOUT_MIN = "factory_reset_timeout_min";

    /**
     * Property key of Timeout size, used in the test configuration file.
     */
    public static final String KEY_TIMEOUT_SIZE = "timeout_size";

    /**
     * Mapping from timeout size to its integer value.
     */
    private static final Map<String, Integer> TIMEOUT_SIZE_MAPPING =
            new HashMap<String, Integer>() {{
                put("S", 1);
                put("M", 2);
                put("L", 3);
                put("XL", 5);
                put("XXL", 8);
            }};

    /**
     * Property key of test timeout in minute, used in the test configuration file.
     */
    public static final String KEY_TEST_TIMEOUT_MIN = "test_timeout_min";

    /**
     * Singleton of this class.
     */
    private static TestConfig sTestConfig;

    // Stores configurations loaded from a file.
    private final Properties mProps;

    /**
     * Creates a new object by loading configurations from file.
     *
     * @param configFile test configuration file
     */
    private TestConfig(File configFile) throws IOException {
        mProps = new Properties();
        mProps.load(new FileInputStream(configFile));
    }

    /**
     * Inits a {@link TestConfig} from a file.
     *
     * @param configFile configuration file to read
     */
    public static void init(File configFile) throws IOException {
        sTestConfig = new TestConfig(configFile);
    }

    /**
     * Gets the singleton of this class.
     *
     * @return {@link TestConfig} singleton, null if it's not initialized.
     */
    public static TestConfig getInstance() {
        return sTestConfig;
    }

    /**
     * Gets the property of a specific key.
     *
     * @param key property key
     * @return propery value of the given key
     *         If the key doesn't exist in the configuration file, null will be returned;
     *         If the value of the key is empty, empty string will be returned
     */
    public String getProperty(String key) {
        return mProps.getProperty(key);
    }

    /**
     * Gets the property of a specific key.
     *
     * @param key property key
     * @param defaultValue default value if given key is not found
     * @return property value of the given key;
     *         if given key not found, {@link #defaultValue} is returned
     */
    public String getProperty(String key, String defaultValue) {
        return mProps.getProperty(key, defaultValue);
    }

    /**
     * Gets factory reset timeout in minute.
     *
     * @return timeout in minute or -1 if either timeout is not specified or invalid
     */
    public int getFactoryResetTimeoutMin() {
        return getFactoryResetTimeoutMin(-1);
    }

    /**
     * Gets factory reset timeout in minute.
     *
     * @param defaultValue default value if test timeout not specified
     * @return factory reset timeout in minute
     */
    public int getFactoryResetTimeoutMin(int defaultValue) {
        String value = mProps.getProperty(KEY_FACTORY_RESET_TIMEOUT_MIN);
        if (value == null || value.isEmpty()) {
            return defaultValue;
        }

        return Integer.valueOf(value);
    }

    /**
     * Gets timeout size value from props file.
     *
     * <p>Possible size values are strings "S", "M", "L", "XL" or "XXL".</p>
     *
     * @return integer value of the timeout size.
     */
    public int getTimeoutSize() {
        // Default to M
        String timeoutSize = getProperty(KEY_TIMEOUT_SIZE, "M");
        if (!TIMEOUT_SIZE_MAPPING.containsKey(timeoutSize)) {
            CLog.w("Invalid timeout size, defaulting to M");
            timeoutSize = "M";
        }

        return TIMEOUT_SIZE_MAPPING.get(timeoutSize);
    }

    /**
     * Gets test timeout in minute.
     *
     * @return test timeout in minute or -1 if either timeout is not specified or invalid
     */
    public int getTestTimeoutMin() {
        return getTestTimeoutMin(-1);
    }

    /**
     * Gets test timeout in minute.
     *
     * @param defaultValue default value if test timeout not specified
     * @return test timeout in minute
     */
    public int getTestTimeoutMin(int defaultValue) {
        String value = mProps.getProperty(KEY_TEST_TIMEOUT_MIN);
        if (value == null || value.isEmpty()) {
            return defaultValue;
        }

        return Integer.valueOf(value);
    }
}

