Android For Work (Afw) Test Harness
-----------------------------------------------------
About

Afw Test Harness is designed to validate Android for Work compatibility of
Android devices. It's a test suite consisting of support apps, test cases,
test runner and configuration files. The test runner, afw-test-tradefed,
is built on top of cts-tradefed. The way of building & running Afw Test
Harness is quite similar to CTS.


Host Machine Setup
------------------
1. Ensure 'adb' is in your current PATH. adb can be found in the
Android SDK available from http://developer.android.com

Example:
  PATH=$PATH:/home/myuser/android-sdk-linux_x86/platform-tools

2. Connect the device to the host machine.

3. Ensure device is visible via 'adb devices'

4. Make sure Java Runtime 1.7 or 1.8 is installed


Afw Test Harness Configurations
-------------------------------
1. Get Testing Work account

Testing work account is required to run the test harness. Testing work account
can be obtained from https://android-for-work-test-harness.appspot.com/.
Please reach out to the Google point of contact within your company to generate
these accounts.
Once obtained the testing account, specify it in the test harness configuration
file, afw-test.props, with the following properties:

work_account_username
work_account_password

2. Configure WIFI network

Configure the Wi-Fi network in afw-test.props with the following properties:

wifi_ssid
wifi_password (optional)
wifi_security_type (optional, available options are: NONE, WEP or WPA)


Build Afw Test Harness
----------------------
Before building or running the harness, init the environment variables:

  $ source build/envsetup.sh
  $ lunch

Select a proper device type and press Enter.

To build Afw Test Harness, browse to this directory and then:

  $ make afw-test-harness -j24

Similar to CTS, "make afw-test-harness" will create a directory,

  out/host/<platform>/afw-th/android-cts

which contains all necessary binaries, configuration files and tools to run the
test suite. This directory is also zipped into a file, android-afw-test-harness.zip,
for distribution.


Run Afw Test Harness
----------------------------------------------
1. From building environment, launch the test runner from the command line:

  $ afw-test-tradefed

2. From the unzipped folder of android-afw-test-harness.zip, launch the test
   runner from the command line:

  $ ./android‐cts/tools/afw-test‐tradefed

  Make sure the ./android‐cts/testcases/afw-test.props has the
  testing work account and WIFI configuration.

3. Run the test plan "afw-userdebug-build": run afw-userdebug-build

   Type 'list plans' to see all the test plans.

   Plan definitions can be found in:
     out/host/<platform>/afw-th/android-cts/tools/afw-test-tradefed.jar/config.

  Each test plan is a xml file which contains all or several test packages from
  AfwTestHarness/tests.

  Plan 'afw-userdebug-build' contains all test packages that require a userdebug build.
  Plan 'afw-user-build' can run on user build but requires the test device to be setup
  properly, including completing Setup Wizard and enabling USB debugging.

4. Run a single test module, for example "AfwTestNfcProvisioningTestCases"

   cts-tf > run cts --module AfwTestNfcProvisioningTestCases

   All modules can be found by executing "list modules" command in
   afw-test-tradefed console.

5. If the device supports both 64 bits & 32 bits abi, you can force the test
   harness to run on a specific abi, e.g.:

   cts-tf > run afw-userdebug-build --abi arm64-v8a -l DEBUG

6.For more options:
   cts-tf > run cts --help
