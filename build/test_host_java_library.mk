# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# Builds a host library and defines a rule to generate the associated test
# package XML needed by CTS.
#
# Replace "include $(BUILD_HOST_JAVA_LIBRARY)" with "include $(BUILD_AFW_TEST_HOST_JAVA_LIBRARY)"
#

include $(BUILD_HOST_JAVA_LIBRARY)
include $(BUILD_AFW_TEST_MODULE_TEST_CONFIG)

afw_test_library_jar := $(AFW_TH_TESTCASES_OUT)/$(LOCAL_MODULE).jar
$(afw_test_library_jar): $(LOCAL_BUILT_MODULE)
	$(copy-file-to-target)

# Have the module name depend on the cts files; so the cts files get generated when you run mm/mmm/mma/mmma.
$(my_register_name) : $(afw_test_library_jar) $(afw_test_module_test_config)
