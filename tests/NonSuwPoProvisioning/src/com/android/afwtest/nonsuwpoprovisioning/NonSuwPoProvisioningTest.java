/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.nonsuwpoprovisioning;

import static org.junit.Assert.assertTrue;

import android.app.admin.DevicePolicyManager;
import android.content.Intent;
import android.support.test.runner.AndroidJUnit4;

import com.android.afwtest.common.AccountManagerUtils;
import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.provisioning.AutomationDriver;
import com.android.afwtest.uiautomator.test.AbstractTestCase;
import com.android.afwtest.uiautomator.test.AfwTestUiWatcher;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Test profile owner provisioning started from Settings.
 */
@RunWith(AndroidJUnit4.class)
public class NonSuwPoProvisioningTest extends AbstractTestCase {

    /**
     * {@inheritDoc}
     */
    @Before
    public void setUp() throws Exception {
        AfwTestUiWatcher.register(getUiDevice());
    }

    /**
     * {@inheritDoc}
     */
    @After
    public void tearDown() throws Exception {
        AfwTestUiWatcher.unregister(getUiDevice());
    }

    /**
     * Tests non-suw profile owner provisioning flow.
     */
    @Test
    public void testNonSuwPoProvisioning() throws Exception {

        // Start Add Account activity.
        AccountManagerUtils.startAddGoogleAccountActivity(getContext(), false);
        AutomationDriver automationDriver = new AutomationDriver(getUiDevice());

        // Navigate provisioning.
        assertTrue("Non-SuW PO provisioning didn't finish",
                automationDriver.runNonSuwPoProvisioning(TestConfig.getDefault()));
    }

    /**
     * Tests provisioning a non-existing package.
     */
    @Test
    public void testDisallowedProvisioning() throws Exception {
        Intent startProvisioning = new Intent(DevicePolicyManager.ACTION_PROVISION_MANAGED_PROFILE);
        // We're asking ManagedProvisioning to provision a non-existing package. It should not be
        // allowed.
        startProvisioning.putExtra(
                DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_PACKAGE_NAME,
                "package.that.does.not.exist");
        startProvisioning.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        AutomationDriver automationDriver = new AutomationDriver(getUiDevice());
        getContext().startActivity(startProvisioning);
        assertTrue("Non-SuW PO provisioning disallowed didn't finish",
                automationDriver.runNonSuwPoProvisioningDisallowed(TestConfig.getDefault()));
    }
}
