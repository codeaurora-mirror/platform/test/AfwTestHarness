/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.pages.gms;

import static com.android.afwtest.uiautomator.Constants.GMS_PKG_NAME_REGEX;
import static java.util.regex.Pattern.CASE_INSENSITIVE;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;
import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.pages.UiPage;
import com.android.afwtest.uiautomator.test.AfwTestUiWatcher;
import com.android.afwtest.uiautomator.utils.Device;
import com.android.afwtest.uiautomator.utils.WidgetUtils;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * GMS Core Google Services page.
 */
public final class GoogleServicesPage extends UiPage {

    /**
     * {@link BySelector} unique to this page.
     */
    private static final BySelector GOOGLE_SERVICES_PAGE_SELECTOR =
            By.pkg(Pattern.compile(GMS_PKG_NAME_REGEX)).text("Google services");

    private static final BySelector GMS_BTN_WITH_TEXT_AGREE =
        By.pkg(Pattern.compile(GMS_PKG_NAME_REGEX))
            .text(Pattern.compile("agree", CASE_INSENSITIVE));

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     */
    public GoogleServicesPage(UiDevice uiDevice, TestConfig config) {
        super(uiDevice, config);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected long getLoadingTimeoutInMs() {
        // Accessing Google server takes long time
        return TimeUnit.MINUTES.toMillis(5);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isOptional() {
        // This page is optional.
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BySelector uniqueElement() {
        return GOOGLE_SERVICES_PAGE_SELECTOR;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void navigate() throws Exception {
        // Otherwise "Agree" will be clicked automatically.
        AfwTestUiWatcher.disallowPageSkipping();

        // Scroll up until "more" button becomes "agree".
        for (int i = 0; i < 5; ++i) {
            Device.swipeUp(getUiDevice());
            if (WidgetUtils.safeWait(getUiDevice(), GMS_BTN_WITH_TEXT_AGREE,
                    TimeUnit.SECONDS.toMillis(3)) != null) {
                break;
            }
        }

        if (!WidgetUtils.safeWaitAndClick(getUiDevice(), GMS_BTN_WITH_TEXT_AGREE)) {
            throw new Exception("Failed to click Agree button");
        }

        AfwTestUiWatcher.allowPageSkipping();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean waitForLoading() throws Exception {
        AfwTestUiWatcher.disallowPageSkipping();
        boolean result = super.waitForLoading();
        AfwTestUiWatcher.allowPageSkipping();
        return result;
    }
}
