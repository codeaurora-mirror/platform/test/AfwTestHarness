/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.pages.testdpc;

import static com.android.afwtest.uiautomator.Constants.STAT_TESTDPC_WORK_PROFILE_CREATION_TIME;
import static com.android.afwtest.uiautomator.Constants.TESTDPC_NEXT_BUTTON_SELECTOR;
import static com.android.afwtest.uiautomator.Constants.TESTDPC_PKG_NAME;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;
import android.widget.RadioButton;

import com.android.afwtest.common.test.TestConfig;
import com.android.afwtest.uiautomator.pages.UiPage;
import com.android.afwtest.uiautomator.utils.WidgetUtils;

import java.util.concurrent.TimeUnit;

/**
 * Setup finished page, with 3 options: add account, add account with name, and Skip.
 */
public final class SetupFinishedPage extends UiPage {
    /** Options for next step.*/
    public enum OPTION {
        DO_NOTHING,
        ADD_ACCOUNT,
        ADD_ACCOUNT_WITH_NAME,
        SKIP,
    };

    /**
     * {@link BySelector} unique to this page.
     */
    private static final BySelector TESTDPC_SETUP_FINISHED_PAGE_SELECTOR =
            By.res(TESTDPC_PKG_NAME, "suw_layout_title")
                    .text("Setup finished");

    /**
     * {@link BySelector} for {@link RadioButton} with resource-id add_account on
     * TestDpc Setup finished page.
     */
    private static final BySelector ADD_ACCOUNT_RADIO_BUTTON =
            By.res(TESTDPC_PKG_NAME, "add_account")
                    .clazz(RadioButton.class)
                    .clickable(true)
                    .checkable(true);

    /**
     * {@link BySelector} for {@link RadioButton} with resource-id add_account_with_name on
     * TestDpc Setup finished page.
     */
    private static final BySelector ADD_ACCOUNT_WITH_NAME_RADIO_BUTTON =
            By.res(TESTDPC_PKG_NAME, "add_account_with_name")
                .clazz(RadioButton.class)
                .clickable(true)
                .checkable(true);

    /**
     * {@link BySelector} for {@link RadioButton} with resource-id add_account_with_name on
     * TestDpc Setup finished page.
     */
    private static final BySelector ADD_ACCOUNT_SKIP_RADIO_BUTTON =
            By.res(TESTDPC_PKG_NAME, "add_account_skip")
                .clazz(RadioButton.class)
                .clickable(true)
                .checkable(true);

    /** Option for next step. */
    private final OPTION mNextStep;

    /**
     * Constructor.
     *
     * @param uiDevice {@link UiDevice} object
     * @param config {@link TestConfig} object holding test configurations
     */
    public SetupFinishedPage(UiDevice uiDevice, TestConfig config, OPTION nextStep) {
        super(uiDevice, config);
        mNextStep = nextStep;
    }

    @Override
    public BySelector uniqueElement() {
        return TESTDPC_SETUP_FINISHED_PAGE_SELECTOR;
    }

    @Override
    public void navigate() throws Exception {
        getProvisioningStatsLogger().stopTime(STAT_TESTDPC_WORK_PROFILE_CREATION_TIME);
        getProvisioningStatsLogger().writeStatsToFile();

        switch(mNextStep) {
            case ADD_ACCOUNT:
                WidgetUtils.waitAndClick(getUiDevice(), ADD_ACCOUNT_RADIO_BUTTON);
                break;
            case ADD_ACCOUNT_WITH_NAME:
                WidgetUtils.waitAndClick(getUiDevice(), ADD_ACCOUNT_WITH_NAME_RADIO_BUTTON);
                break;
            case SKIP:
                WidgetUtils.waitAndClick(getUiDevice(), ADD_ACCOUNT_SKIP_RADIO_BUTTON);
                break;
        }
        if (!mNextStep.equals(OPTION.DO_NOTHING)) {
            WidgetUtils.waitAndClick(getUiDevice(), TESTDPC_NEXT_BUTTON_SELECTOR);
        }
    }
}
