/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.afwtest.uiautomator;

import static java.util.regex.Pattern.CASE_INSENSITIVE;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Switch;

import java.util.regex.Pattern;

/**
 * Constants for this lib and test packages.
 */
public final class Constants {

    /**
     * Android package name.
     */
    public static final String ANDROID_PKG_NAME = "android";

    /**
     * System UI package name.
     */
    public static final String SYSTEM_UI_PKG_NAME = "com.android.systemui";

    /**
     * GMSCore package name.
     */
    public static final String GMS_PKG_NAME = "com.google.android.gms";

    /**
     * GMSCore package name regular expression string.
     */
    public static final String GMS_PKG_NAME_REGEX =
            Pattern.quote(GMS_PKG_NAME) + "(\\.[^:]+)?";

    /**
     * Managed provisioning package name.
     */
    public static final String MANAGED_PROVISIONING_PKG_NAME = "com.android.managedprovisioning";

    /**
     * Android package installer package name.
     */
    public static final String PACKAGE_INSTALLER_PKG_NAME = "com.android.packageinstaller";

    /**
     * TestDpc package name.
     */
    public static final String TESTDPC_PKG_NAME = "com.afwsamples.testdpc";

    /**
     * Default file name for provisioning performance stats.
     */
    public static final String PROVISIONING_STATS_FILE = "Provisioning-Stats.csv";

    /**
     * Representation name of TestDpc work profile creation time.
     */
    public static final String STAT_TESTDPC_WORK_PROFILE_CREATION_TIME =
            "testDPC Work Profile creation";

    /**
     * Representation name of Managed Provisioning work profile creation name.
     */
    public static final String STAT_MANAGED_PROVISIONING_WORK_PROFILE_CREATION_TIME =
            "MP Work Profile creation";

    /**
     * GMS button with text ok.
     */
    public static final BySelector GMS_BTN_WITH_TEXT_OK =
        By.pkg(GMS_PKG_NAME).text(Pattern.compile("ok", CASE_INSENSITIVE));

    /**
     * GMS button with content description ok.
     */
    public static final BySelector GMS_BTN_WITH_DESC_OK =
        By.pkg(GMS_PKG_NAME).desc(Pattern.compile("ok", CASE_INSENSITIVE));

    /**
     * Regular expression string to match resource id of GMSCore NEXT button.
     */
    public static final String GMS_NEXT_BTN_RESOURCE_ID_REGEX =
            GMS_PKG_NAME_REGEX
                    + ":id/(auth_setup_wizard_navbar_next"
                    + "|suw_navbar_next"
                    + "|google_services_next_button_item"
                    + "|auth_device_management_download_next_button"
                    + "|next_button)";
    /**
     * {@link BySelector} for {@link Button} in GMS core with description "ACCEPT".
     */
    public static final BySelector GMS_ACCEPT_BUTTON_SELECTOR =
        By.pkg(GMS_PKG_NAME)
            .desc(Pattern.compile("ACCEPT", CASE_INSENSITIVE));

    /**
     * {@link BySelector} for {@link Button} in GMS core with text "NEXT".
     */
    public static final BySelector GMS_BUTTON_WITH_TEXT_NEXT =
            By.pkg(GMS_PKG_NAME)
                    .text(Pattern.compile("NEXT", CASE_INSENSITIVE));


    /**
     * {@link BySelector} for {@link Button} in GMS core with description "NEXT".
     */
    public static final BySelector GMS_NEXT_BUTTON_SELECTOR =
        By.pkg(GMS_PKG_NAME)
            .desc(Pattern.compile("NEXT", CASE_INSENSITIVE));

    /**
     * Resource Id {@link BySelector} for GmsCore NEXT button.
     */
    public static final BySelector GMS_NEXT_BUTTON_RES_SELECTOR =
            By.res(Pattern.compile(GMS_NEXT_BTN_RESOURCE_ID_REGEX))
                    .enabled(true)
                    .clickable(true);

    /**
     * {@link BySelector} for {@link EditText} in GMS core.
     */
    public static final BySelector GMS_TEXT_FIELD_SELECTOR = By.pkg(GMS_PKG_NAME).clazz(
            EditText.class.getName());

    /**
     * {@link BySelector} for {@link CheckBox} in GMS core with resource-id "agree_backup".
     */
    public static final BySelector GMS_AGREE_BACKUP_SELECTOR =
            By.res(Pattern.compile(GMS_PKG_NAME_REGEX + ":id/agree_backup"));

    /**
     * {@link BySelector} for {@link Switch} in GMS core with resource-id
     * "suw_items_switch".
     */
    public static final BySelector GMS_SWITCH_SELECTOR =
            By.res(Pattern.compile(GMS_PKG_NAME_REGEX + ":id/suw_items_switch"));

    /**
     * {@link BySelector} for {@link Button} in GMS core with text "CLOSE".
     */
    public static final BySelector GMS_CLOSE_BACKUP_MESSAGE_SELECTOR =
            By.pkg(GMS_PKG_NAME).text("CLOSE");

    /**
     * @{@link BySelector} for downloading MDMs in GmsCore.
     */
    public static final BySelector GMS_DOWNLOADING_DIALOG_SELECTOR =
            By.text(Pattern.compile("Downloading.*"))
                    .res(ANDROID_PKG_NAME, "message")
                    .pkg(GMS_PKG_NAME);

    /**
     * {@link BySelector} for GMSCore web view UI.
     */
    public static final BySelector GMS_WEBVIEW_SELECTOR =
            By.pkg(Constants.GMS_PKG_NAME).clazz(WebView.class.getName());

    /**
     * @{@link BySelector} unique to the add account page of GmsCore.
     */
    public static final BySelector GMS_ADD_ACCOUNT_PAGE_SELECTOR =
            By.pkg(GMS_PKG_NAME)
                .desc(Pattern.compile("Add your account|Sign in", CASE_INSENSITIVE));

    /**
     * {@link BySelector} for {@link EditText} with resource-name "password" in GMSCore.
     */
    public static final BySelector GMS_PASSWORD_FIELD_SELECTOR =
            By.pkg(GMS_PKG_NAME).res("password");

    /**
     * {@link BySelector} for {@link Button} in ManagedProvisioning to set up the profile/device.
     */
    public static final BySelector MANAGED_PROVISIONING_SETUP_BUTTON_SELECTOR = By.res(
            Pattern.compile(MANAGED_PROVISIONING_PKG_NAME + ":id/next_button"));

    /**
     * {@link BySelector} in ManagedProvisioning.
     */
    public static final BySelector MANAGED_PROVISIONING_PKG_SELECTOR = By.pkg(
            MANAGED_PROVISIONING_PKG_NAME);

    /**
     * {@link BySelector} for {@link Button} in ManagedProvisioning with resource-id
     * positive_button.
     */
    public static final BySelector MANAGED_PROVISIONING_OK_BUTTON_SELECTOR = By.res(
            MANAGED_PROVISIONING_PKG_NAME, "positive_button");

    public static final BySelector MANAGED_PROVISIONING_SCROLL_VIEW_SELECTOR =
            By.pkg(MANAGED_PROVISIONING_PKG_NAME).clazz(ScrollView.class.getName());

    /**
     * {@link BySelector} for {@link Button} in Package Installer with resource-id ok_button.
     */
    public static final BySelector PACKAGE_INSTALLER_INSTALL_BUTTON_SELECTOR = By.res(
            PACKAGE_INSTALLER_PKG_NAME, "ok_button");

    public static final BySelector TESTDPC_SETUP_MANAGEMENT_PAGE_TITLE_SELECTOR =
            By.pkg(TESTDPC_PKG_NAME).text(Pattern.compile("Set.*management.*", CASE_INSENSITIVE));

    public static final BySelector TESTDPC_SCROLL_VIEW_SELECTOR =
            By.pkg(TESTDPC_PKG_NAME).clazz(ScrollView.class.getName());

    /**
     * {@link BySelector} for {@link RadioButton} with resource-id setup_device_owner on
     * TestDpc Setup Management page.
     */
    public static final BySelector TESTDPC_SETUP_DEVICE_OWNER_RADIO_BUTTON_SELECTOR =
            By.res(TESTDPC_PKG_NAME, "setup_device_owner")
                    .clazz(RadioButton.class.getName())
                    .clickable(true)
                    .checkable(true);

    /**
     * {@link BySelector} for {@link RadioButton} with resource-id setup_managed_profile on
     * TestDpc Setup Management page.
     */
    public static final BySelector TESTDPC_SETUP_MANAGED_PROFILE_RADIO_BUTTON_SELECTOR =
            By.res(TESTDPC_PKG_NAME, "setup_managed_profile")
                    .clazz(RadioButton.class.getName())
                    .clickable(true)
                    .checkable(true);

    /**
     * {@link BySelector} for the 'NEXT' navigate button on TestDpc pages.
     */
    public static final BySelector TESTDPC_NEXT_BUTTON_SELECTOR =
            By.res(TESTDPC_PKG_NAME, "suw_navbar_next")
                    .clazz(Button.class.getName())
                    .clickable(true);

    /**
     * {@link BySelector} for Managed Provisioning error message.
     */
    public static final BySelector MANAGED_PROVISIONING_ERROR_MSG_SELECTOR =
            By.pkg(MANAGED_PROVISIONING_PKG_NAME).res(ANDROID_PKG_NAME, "message");

    /**
     * {@link BySelector} for Android platform alert message.
     */
    public static final BySelector ANDROID_ALERT_MSG_SELECTOR = By.res(ANDROID_PKG_NAME, "message");

    /**
     * {@link BySelector} for Android platform button.
     */
    public static final BySelector ANDROID_DIALOG_BTN_SELECTOR = By.clazz(Button.class.getName());

    /**
     * {@link BySelector} for {@link Button} in TestDpc with resource-id finish_setup.
     */
    public static final BySelector TESTDPC_FINISH_SKIP_BUTTON_SELECTOR =
            By.res(TESTDPC_PKG_NAME, "btn_add_account_skip");

    /**
     * Private constructor to prevent instantiation.
     */
    private Constants() {
    }
}
