/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.test;

import android.app.Instrumentation;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.uiautomator.UiDevice;

/**
 * Abstract class for test cases using android-support-test.
 *
 * Provides common methods that are likely to be needed in test cases.
 */
public abstract class AbstractTestCase {

    /**
     * Gets application context.
     *
     * @return application context
     */
    protected Context getContext() {
        return InstrumentationRegistry.getContext();
    }

    /**
     * Get current instance of {@link UiDevice}.
     *
     * @return current {@link UiDevice}
     */
    protected UiDevice getUiDevice() {
        return UiDevice.getInstance(getInstrumentation());
    }

    /**
     * Gets current instance of {@link Instrumentation}.
     *
     * @return current {@link Instrumentation}
     */
    public Instrumentation getInstrumentation() {
        return InstrumentationRegistry.getInstrumentation();
    }
}
