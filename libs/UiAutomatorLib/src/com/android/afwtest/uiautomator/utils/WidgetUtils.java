/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.afwtest.uiautomator.utils;

import android.graphics.Rect;
import android.os.SystemClock;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.Direction;
import android.support.test.uiautomator.StaleObjectException;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.util.Log;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * Widget utils.
 */
public class WidgetUtils {

    private static final String TAG = "afwtest.WidgetUtils";

    /**
     * Waiting time for each call to UiDevice.wait().
     */
    private static final long DEFAULT_UI_WAIT_TIME_MS = TimeUnit.SECONDS.toMillis(5);

    /**
     * Number of attempts to call {@link UiDevice#wait()} to avoid {@link StaleObjectException}.
     */
    private static final int DEFAULT_UI_ATTEMPTS_COUNT = 3;

    /**
     * Margin of swipe's starting and ending points inside the target.
     */
    private static final float DEFAULT_SWIPE_DEADZONE_PCT = 0.1f;

    /**
     * Swipe's length as object's dimension percentage.
     */
    private static final float DEFAULT_SWIPE_PCT = 0.7f;

    /**
     * Keyboard pop up waiting time, in milliseconds.
     */
    private static final long KEYBOARD_START_TIME_MS = TimeUnit.SECONDS.toMillis(2);

    /**
     * Shell command to execute in order to determine if keyboard is visible.
     */
    private static final String FIND_KEYBOARD_COMMAND = "dumpsys input_method";

    /**
     * String present in dumpsys command's output when keyboard is visible.
     */
    private static final String KEYBOARD_IS_SHOWN_STRING = "mInputShown=true";

    /**
     * Clicks on given {@link UiObject2} without throwing any exception.
     *
     * @param obj {@link UiObject2} to click
     * @return {@code true} if clicked, {@code false} otherwise
     */
    public static boolean safeClick(UiObject2 obj) {
        String widgetProps = getWidgetPropertiesAsString(obj);
        try {
            obj.click();
            Log.d(TAG, String.format("Clicked: %s", widgetProps));
            return true;
        } catch(Exception e) {
            Log.e(TAG, String.format("Failed to click: %s", widgetProps) , e);
            return false;
        }
    }

    /**
     * Tries to click any of the given list of buttons; return if any button
     * clicked successfully.
     *
     * @param btns list of buttons to click
     * @return {@code true} if any button clicked successfully; {@code false} otherwise
     */
    public static boolean safeClickAny(List<UiObject2> btns) {
        for (UiObject2 obj : btns) {
            if (safeClick(obj)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Perform fling gesture on given {@link UiObject2} until it cannot scroll any more without
     * throwing any exception.
     *
     * @param obj {@link UiObject2} to scroll
     * @param direction The direction in which to fling
     * @return {@code true} if fling performed, {@code false} otherwise
     */
    public static boolean safeFling(UiObject2 obj, Direction direction) {
        String widgetProps = getWidgetPropertiesAsString(obj);

        try {
            // Set limit to 100 times.
            for (int i = 0; i < 100; ++i) {
                if (!obj.fling(direction)) {
                    break;
                }
            }
            Log.d(TAG, String.format("Scrolled: %s", widgetProps));
            return true;
        } catch(Exception e) {
            Log.e(TAG, String.format("Failed to scroll: %s", widgetProps), e);
            return false;
        }
    }

    /**
     * Waits for a widget without throwing any exception (N attempts).
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the widget to wait
     * @param timeoutMS timeout in milliseconds
     * @param attempts number of attempts.
     * @return {@link UiObject2} if expected widget appears within timeout, {@code null} otherwise
     */
    public static UiObject2 safeWait(UiDevice uiDevice, BySelector selector, long timeoutMS,
            int attempts) {
        for (int i = 0; i < attempts; ++i) {
            UiObject2 widget = safeWait(uiDevice, selector, timeoutMS);
            if (widget != null) {
                return  widget;
            }
        }

        return null;
    }

    /**
     * Waits for a widget without throwing any exception.
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the widget to wait
     * @param timeoutMS timeout in milliseconds
     * @return {@link UiObject2} if expected widget appears within timeout, {@code null} otherwise
     */
    public static UiObject2 safeWait(UiDevice uiDevice, BySelector selector, long timeoutMS) {
        try {
            return uiDevice.wait(Until.findObject(selector), timeoutMS);
        } catch (Exception e) {
            Log.e(TAG, "Failed to wait for widget ", e);
        }

        return null;
    }

    /**
     * Waits for a widget without throwing any exception.
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the widget to wait
     * @return {@link UiObject2} if expected widget appears within default timeout,
     *         {@code null} otherwise
     */
    public static UiObject2 safeWait(UiDevice uiDevice, BySelector selector) {
        return safeWait(uiDevice, selector, DEFAULT_UI_WAIT_TIME_MS, DEFAULT_UI_ATTEMPTS_COUNT);
    }

    /**
     * Waits for a widget and click on it without throwing any exception.
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the widget to wait
     * @return {@code true} if click action was performed, {@code false} otherwise.
     */
    public static boolean safeWaitAndClick(UiDevice uiDevice, BySelector selector) {
        try {
            waitAndClick(uiDevice, selector);
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Failed to wait and click for widget ", e);
        }

        return false;
    }

    /**
     * Waits for a widget and click on it.
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the widget to wait
     */
    public static void waitAndClick(UiDevice uiDevice, BySelector selector)
            throws Exception {
        waitAndClick(uiDevice, selector, DEFAULT_UI_WAIT_TIME_MS, DEFAULT_UI_ATTEMPTS_COUNT);
    }

    /**
     * Waits and click on a child widget without throwing any exception.
     *
     * @param uiDevice {@link UiDevice} object
     * @param parent {@link BySelector} of the parent widget to wait
     * @param child {@link BySelector} of the child widget to wait
     * @param timeoutMS timeout in milliseconds
     * @return {@code true} if click action was performed, {@code false} otherwise
     */
    public static boolean safeWaitAndClick(UiDevice uiDevice, BySelector parent, BySelector child,
            long timeoutMS) {
        try {
            waitAndClick(uiDevice, parent, child, timeoutMS);
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Failed to wait and click on child widget ", e);
        }

        return false;
    }

    /**
     * Waits and click on a child widget.
     *
     * @param uiDevice {@link UiDevice} object
     * @param parent {@link BySelector} of the parent widget to wait
     * @param child {@link BySelector} of the child widget to wait
     * @param timeoutMS timeout in milliseconds
     */
    public static void waitAndClick(UiDevice uiDevice, BySelector parent, BySelector child,
            long timeoutMS) throws Exception {
        UiObject2 object = wait(uiDevice, parent, child, timeoutMS);
        if (object == null) {
            throw new Exception(String.format("Child widget(%s) not found", child.toString()));
        }
        object.click();
    }

    /**
     * Waits for a child object into a parent widget.
     *
     * @param uiDevice {@link UiDevice} object
     * @param parent {@link BySelector} of the parent widget to wait
     * @param child {@link BySelector} of the child widget to wait
     * @param timeoutMS timeout in milliseconds
     * @return {@link UiObject2} if expected widget appears within timeout, {@code null} otherwise
     */
    public static UiObject2 wait(UiDevice uiDevice, BySelector parent, BySelector child,
            long timeoutMS) throws Exception {

        UiObject2 parentWidget = WidgetUtils.safeWait(uiDevice, parent, timeoutMS);
        if (parentWidget == null) {
            throw new Exception(String.format("Parent(%s) widget not found", parent.toString()));
        }

        return parentWidget.findObject(child);
    }

    /**
     * Waits for a widget and click on it without throwing any exception.
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the widget to wait
     * @param timeoutMS timeout in milliseconds
     * @return {@code true} if click action was performed, {@code false} otherwise.
     */
    public static boolean safeWaitAndClick(UiDevice uiDevice, BySelector selector, long timeoutMS) {
        try {
            waitAndClick(uiDevice, selector, timeoutMS);
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Failed to wait and click for widget ", e);
        }

        return false;
    }

    /**
     * Waits for a widget and click on it (N attempts).
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the widget to wait
     * @param timeoutMS timeout in milliseconds
     * @param attempts number of attempts
     */
    public static void waitAndClick(UiDevice uiDevice, BySelector selector, long timeoutMS,
            int attempts) throws Exception {
        for (int i = 0; i < attempts; ++i) {
            if(safeWaitAndClick(uiDevice, selector, timeoutMS)) {
                return;
            }
        }

        throw new Exception(String.format("UI object not found: %s after %d attempts",
                selector.toString(),
                attempts));
    }

    /**
     * Waits for a widget and click on it.
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the widget to wait
     * @param timeoutMS timeout in milliseconds
     */
    public static void waitAndClick(UiDevice uiDevice, BySelector selector, long timeoutMS)
            throws Exception {
        UiObject2 object = uiDevice.wait(Until.findObject(selector), timeoutMS);
        if (object == null) {
            throw new Exception(String.format("UI object not found: %s", selector.toString()));
        }
        object.click();
    }

    /**
     * Waits for all elements with given {@link BySelector} to be gone.
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the UI elements to wait
     * @param timeoutMs timeout in mmilliseconds
     */
    public static void waitToBeGone(UiDevice uiDevice, BySelector selector, long timeoutMs)
            throws Exception {
        uiDevice.wait(Until.gone(selector), timeoutMs);
    }

    /**
     * Waits for all elements with given {@link BySelector} to be gone.
     *
     * @param uiDevice {@link UiDevice} object
     * @param selector {@link BySelector} of the UI elements to wait
     */
    public static void waitToBeGone(UiDevice uiDevice, BySelector selector) throws Exception {
        waitToBeGone(uiDevice, selector, DEFAULT_UI_WAIT_TIME_MS);
    }

    /**
     * Scrolls a scrollable UI widget so that an item with certain {@link TextView} text is in view.
     *
     * @param container Selector for the scrollable widget.
     * @param item Text that appears in the target item.
     * @return The target item as a {@link UiObject} when it first appears, or {@code null} if
     *         not found.
     * @throws UiObjectNotFoundException If the scrollable does not exist.
     */
    public static UiObject scrollToItem(UiSelector container, String item)
            throws UiObjectNotFoundException {
        UiSelector itemSelector = new UiSelector().className(TextView.class).text(item);
        return scrollToItem(container, itemSelector);
    }

    /**
     * Scrolls a scrollable UI widget so that a certain item is in view.
     *
     * @param container Selector for the scrollable widget.
     * @param item Selector that specifies the target item.
     * @return The target item as a {@link UiObject} when it first appears, or {@code null} if
     *         not found.
     * @throws UiObjectNotFoundException If the scrollable does not exist.
     */
    public static UiObject scrollToItem(UiSelector container, UiSelector item)
            throws UiObjectNotFoundException {
        UiScrollable scrollable = new UiScrollable(container);
        if (!scrollable.waitForExists(DEFAULT_UI_WAIT_TIME_MS)) {
            throw new UiObjectNotFoundException("Cannot find scrollable " + scrollable);
        }

        if (scrollable.scrollIntoView(item)) {
            return scrollable.getChild(item);
        } else {
            return null;
        }
    }

    /**
     * Scrolls vertically a scrollable UI widget so that a certain item is in view.
     *
     * @param uiDevice current {@link UiDevice}.
     * @param container {@link BySelector} of a scrollable container.
     * @param item {@link BySelector} of an item to scroll to.
     * @return The target item as a {@link UiObject2} when it first appears, or {@code null} if
     *         not found.
     * @throws UiObjectNotFoundException If the scrollable does not exist.
     */
    public static UiObject2 scrollToItem(UiDevice uiDevice,
            BySelector container,
            BySelector item) throws UiObjectNotFoundException {
        return scrollToItem(uiDevice, container, item, false);
    }

    /**
     * Scrolls a scrollable UI widget so that a certain item is in view.
     *
     * @param uiDevice current {@link UiDevice}.
     * @param container {@link BySelector} of a scrollable container.
     * @param item {@link BySelector} of an item to scroll to.
     * @param isHorizontal {@code true} if scrollable is horizontal, {@code false} otherwise.
     * @return The target item as a {@link UiObject2} when it first appears, or {@code null} if
     *         not found.
     * @throws UiObjectNotFoundException If the scrollable does not exist.
     */
    public static UiObject2 scrollToItem(UiDevice uiDevice,
            BySelector container,
            BySelector item,
            boolean isHorizontal) throws UiObjectNotFoundException {

        // Find scrollable object
        final UiObject2 scrollable = safeWait(uiDevice, container);
        if (scrollable == null) {
            throw new UiObjectNotFoundException("Cannot find scrollable " + container);
        }

        setGestureMargins(scrollable, DEFAULT_SWIPE_DEADZONE_PCT);

        // Determine scrolling direction
        final Direction scrollDirection = isHorizontal ? Direction.RIGHT : Direction.DOWN;
        final Direction oppositeDirection = Direction.reverse(scrollDirection);

        // Scroll to the beginning of the scrollable
        while (scrollable.scroll(oppositeDirection, DEFAULT_SWIPE_PCT));

        // Scroll while target item is not visible and scrolling is still possible
        UiObject2 foundItem = scrollable.findObject(item);
        boolean isScrollingPossible = true;
        while (foundItem == null && isScrollingPossible) {
            isScrollingPossible = scrollable.scroll(scrollDirection, DEFAULT_SWIPE_PCT);
            foundItem = scrollable.findObject(item);
        }
        return foundItem;
    }

    /**
     * Scrolls a scrollable UI widget so that a certain item is in view and clicks this item.
     *
     * @param uiDevice current {@link UiDevice}.
     * @param container {@link BySelector} of a scrollable container.
     * @param item {@link BySelector} of an item to scroll to.
     * @throws UiObjectNotFoundException if either scrollable or item could not be found.
     */
    public static void scrollToItemAndClick(UiDevice uiDevice,
            BySelector container,
            BySelector item) throws UiObjectNotFoundException {

        scrollToItemAndClick(uiDevice, container, item, false);
    }

    /**
     * Scrolls a scrollable UI widget so that a certain item is in view and clicks this item.
     *
     * @param uiDevice current {@link UiDevice}.
     * @param container {@link BySelector} of a scrollable container.
     * @param item {@link BySelector} of an item to scroll to.
     * @param isHorizontal {@code true} if scrollable is horizontal, {@code false} otherwise.
     * @throws UiObjectNotFoundException if either scrollable or item could not be found.
     */
    public static void scrollToItemAndClick(UiDevice uiDevice,
            BySelector container,
            BySelector item,
            boolean isHorizontal) throws UiObjectNotFoundException {

        UiObject2 foundItem = scrollToItem(uiDevice, container, item, isHorizontal);
        if (foundItem == null) {
            throw new UiObjectNotFoundException(item.toString() + " could not be found.");
        }
        foundItem.click();
    }

    /**
     * Set target object's gesture margins.
     * @param target target object.
     * @param marginPct gesture margin as target's dimension percentage.
     */
    private static void setGestureMargins(UiObject2 target, float marginPct) {
        final Rect bounds = callWithRetry(target::getVisibleBounds, DEFAULT_UI_ATTEMPTS_COUNT);
        final int horizontalMargin = (int)(bounds.width() * marginPct);
        final int verticalMargin = (int)(bounds.height() * marginPct);
        target.setGestureMargins(horizontalMargin,
                verticalMargin,
                horizontalMargin,
                verticalMargin);
    }

    /**
     * Gets properties of a {@link UiObject2} as a String, for debugging purpose.
     *
     * @param widget {@link UiObject2} to get properties from
     * @return properties of given {@link UiObject2} as String
     */
    public static String getWidgetPropertiesAsString(UiObject2 widget) {
        try {
            return String.format("text=[%s],desc=[%s],res=[%s],pkg=[%s],class=[%s]",
                    widget.getText(),
                    widget.getContentDescription(),
                    widget.getResourceName(),
                    widget.getApplicationPackage(),
                    widget.getClassName());
        } catch (Exception e) {
            Log.e(TAG, "Failed to get properties from a widget", e);
        }

        return null;
    }

    /**
     * Gets the package name of a {@link UiObject2} safely.
     *
     * @param widget {@link UiObject2} to get property from
     * @return package name of given widget or null if there is any error
     */
    public static String getPackageName(UiObject2 widget) {
        try {
            return widget.getApplicationPackage();
        } catch (Exception e) {
            Log.e(TAG, "Failed to get package name from a widget", e);
        }

        return null;
    }

    /**
     * Gets the text of a {@link UiObject2} safely.
     *
     * @param widget {@link UiObject2} to get property from
     * @return text of given widget or null if there is any error
     */
    public static String getText(UiObject2 widget) {
        try {
            return widget.getText();
        } catch (Exception e) {
            Log.e(TAG, "Failed to get text from a widget", e);
        }

        return null;
    }

    /**
     * Gets the content description of a {@link UiObject2} safely.
     *
     * @param widget {@link UiObject2} to get property from
     * @return content description of given widget or null if there is any error
     */
    public static String getContentDescription(UiObject2 widget) {
        try {
            return widget.getContentDescription();
        } catch (Exception e) {
            Log.e(TAG, "Failed to get text from a widget", e);
        }

        return null;
    }

    /**
     * Presses back until element specified by either {@code targetSelector}
     * or {@code limiterSelector} is found. If element specified by {@code targetSelector} is found,
     * it is returned. {@code null} is returned otherwise.
     *
     * @param uiDevice current {@link UiDevice}.
     * @param targetSelector {@link BySelector} of element to found.
     * @param limiterSelector {@link BySelector} of boundary element to represent failed search.
     * @param maxAttempts Max number of attempts
     * @return element specified by {@code targetSelector} if found, {@code null} otherwise.
     */
    public static UiObject2 safeGoBackUntilFound(UiDevice uiDevice, BySelector targetSelector,
            BySelector limiterSelector, int maxAttempts) {
        UiObject2 foundObject = null;
        while (maxAttempts-- > 0
                && (foundObject = WidgetUtils.safeWait(uiDevice, targetSelector)) == null
                && WidgetUtils.safeWait(uiDevice, limiterSelector) == null) {
            uiDevice.pressBack();
        }
        return foundObject;
    }

    /**
     * Presses back until element specified by either {@code targetSelector}
     * or {@code limiterSelector} is found. If element specified by {@code targetSelector} is found,
     * it is returned. {@code null} is returned otherwise.
     *
     * @param uiDevice current {@link UiDevice}.
     * @param targetSelector {@link BySelector} of element to found.
     * @param limiterSelector {@link BySelector} of boundary element to represent failed search.
     * @return element specified by {@code targetSelector} if found, {@code null} otherwise.
     */
    public static UiObject2 safeGoBackUntilFound(UiDevice uiDevice, BySelector targetSelector,
        BySelector limiterSelector) {
        return safeGoBackUntilFound(uiDevice, targetSelector, limiterSelector, 10);
    }

    /**
     * Waits until keyboard is visible.
     *
     * @param uiDevice current {@link UiDevice}.
     * @param maxAttempts maximum attempts to find keyboard before reporting failure.
     * @return {@code true} if keyboard was found and is visible, {@code false} otherwise.
     * @throws IOException if could not run shell command to get dumpsys info.
     */
    public static boolean waitForKeyboard(UiDevice uiDevice, int maxAttempts) throws IOException {
        while ((maxAttempts--) > 0) {
            SystemClock.sleep(KEYBOARD_START_TIME_MS);
            final String output = uiDevice.executeShellCommand(FIND_KEYBOARD_COMMAND);
            if (output.contains(KEYBOARD_IS_SHOWN_STRING)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Executes provided action. In case the call throws
     * {@link StaleObjectException} it is retried at most {@code maxAttempts} times.
     *
     * @param action action to execute.
     * @param maxAttempts maximum attempt to perform.
     * @param <T> return type of provided action.
     * @return value returned by provided action.
     */
    public static <T> T callWithRetry(Supplier<T> action, int maxAttempts) {
        if (maxAttempts <= 0) {
            throw new IllegalArgumentException("maxAttempts must be positive integer value.");
        }

        T result = null;
        while ((maxAttempts--) > 0) {
            try {
                result = action.get();
                break;
            } catch (StaleObjectException e) {
                if (maxAttempts == 0) {
                    throw e;
                }
            }
        }

        return result;
    }
}
